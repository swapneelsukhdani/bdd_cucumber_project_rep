Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sun, 17 Mar 2024 22:22:58 GMT

Response body is : 
{"name":"morpheus","job":"leader","updatedAt":"2024-03-17T22:22:58.209Z"}
Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Sukhdani",
    "job": "Tester"
}

Response header date is : 
Sun, 17 Mar 2024 22:22:54 GMT

Response body is : 
{"name":"Sukhdani","job":"Tester","id":"347","createdAt":"2024-03-17T22:22:54.863Z"}
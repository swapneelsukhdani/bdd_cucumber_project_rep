Feature: Trigger Put API
@Put_API_TestCases
Scenario: Trigger the API Request with String Request Body Parameters
          Given Name and Job in Request Body in Put API
          When Send the Request with PayLoad to the endpoint in Put API
          Then Validate Status Code in Put API
          And Validate Response Body Parameters in Put API
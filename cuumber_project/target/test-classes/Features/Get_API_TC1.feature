Feature: Trigger Get API

@Get_API_Testcases
Scenario: Trigger the API Request with String Request Body Parameters
          Given Name and Job in Request Body in get api
          When Send the Request with PayLoad to the endpoint in get api
          Then Validate Status Code in get api
          And Validate Response Body Parameters in get api
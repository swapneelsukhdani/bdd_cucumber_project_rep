Feature: Trigger Delete API

@Delete_API_TestCases
Scenario: Trigger the API Request with String Request Body Parameters
          Given Name and Job in Request Body in delete
          When Send the Request with PayLoad to the endpoint in delete
          Then Validate Status Code in delete
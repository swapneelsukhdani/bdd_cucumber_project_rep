Feature: Trigger the Post API and validate the REsponse Body and Response Parameters

@Post_API_Testcases
Scenario: Trigger the API Request with String Request Body Parameters
          Given Name and Job in Request Body
          When Send the Request with PayLoad to the endpoint
          Then Validate Status Code
          And Validate Response Body Parameters
          
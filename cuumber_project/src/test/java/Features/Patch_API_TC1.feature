Feature: Trigeer the Patch API

@Patch_API_Testcases
Scenario: Trigger the API Request with String Request Body Parameters
          Given Name and Job in Request Body in patch
          When Send the Request with PayLoad to the endpoint in patch
          Then Validate Status Code in patch
          And Validate Response Body Parameters in patch
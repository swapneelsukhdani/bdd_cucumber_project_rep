Feature: Trigger Post API on the basis of Input Data

@DataDriven
Scenario Outline: Trigger the Post API Request with Vailid Request Parameters
          Given "<Name>" and "<Job>" in Request Body
          When Send the Request with PayLoad to the endpoint
          Then Validate Status Code
          And Validate Response Body Parameters
          
Examples: 

         |Name |Job |
         |Swapneel|QA|
         |Sukhdani|Tester| 
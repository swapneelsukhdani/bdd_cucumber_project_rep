package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class GetstepDefination {

	
	Response response;
	String endpoint;
	File dir_name;
	int statuscode;
	
	String res_email;
	String res_firstName;
	String res_lastName;
	String res_url;
	String res_text;
	String requestBody;
	String Endpoint;
	String exp_id="[7, 8, 9, 10, 11, 12]";
    String res_id;
    String exp_email="[michael.lawson@reqres.in, lindsay.ferguson@reqres.in, tobias.funke@reqres.in, byron.fields@reqres.in, george.edwards@reqres.in, rachel.howell@reqres.in]";

	@Given("Name and Job in Request Body in get api")
	public void name_and_job_in_request_body_in_get_api() throws IOException {
		dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		requestBody = RequestBody.req_get_tc("Get_TC1");
		Endpoint = Environment.Hostname() + Environment.Resource_get() + requestBody;

		response = API_Trigger.Get_trigger(Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Get_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

		
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Request with PayLoad to the endpoint in get api")
	public void send_the_request_with_pay_load_to_the_endpoint_in_get_api() {
		statuscode = response.getStatusCode();
		res_id = response.getBody().jsonPath().getString("data.id");
		res_email = response.getBody().jsonPath().getString("data.email");
	}

	@Then("Validate Status Code in get api")
	public void validate_status_code_in_get_api() {
		Assert.assertEquals(statuscode, 200);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Response Body Parameters in get api")
	public void validate_response_body_parameters_in_get_api() {
		ResponseBody res_body = response.getBody();
		Assert.assertEquals(res_id, exp_id );
		Assert.assertEquals(res_email, exp_email);
		}

}

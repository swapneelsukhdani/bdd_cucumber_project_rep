package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
//import junit.framework.Assert;

//import junit.framework.Assert;
	

public class PoststepDefination {
	
	File dir_name;
	String requestBody;
	String Endpoint;
	int statuscode;
	Response response;
	
	ResponseBody res_body;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	
	String req_name;
	String req_job;
	String expecteddate;
	
	
	@Before
	public void Setup() {
		
	System.out.println("Before gettling open");
	}
	@Given("{string} and {string} in Request Body")
	public void and_in_request_body(String req_name, String req_job) throws IOException {
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		Endpoint = Environment.Hostname() + Environment.Resource_create();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());		
	}
	
	@After
	public void Setup1() {
		
		System.out.println("After gettling open");
	}
	@Given("Name and Job in Request Body")
	public void name_and_job_in_request_body() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_post_tc("Post_TC1");
		Endpoint = Environment.Hostname() + Environment.Resource_create();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	//    throw new io.cucumber.java.PendingException();
	}
	
	@When("Send the Request with PayLoad to the endpoint")
	public void send_the_request_with_pay_load_to_the_endpoint() {
		
			statuscode = response.statusCode();
			//LocalDateTime currentdate = LocalDateTime.now();
		//	expecteddate = currentdate.toString().substring(0, 11);
			res_body = response.getBody();
			res_name = res_body.jsonPath().getString("name");
			res_job = res_body.jsonPath().getString("job");
			res_id = res_body.jsonPath().getString("id");
			res_createdAt = res_body.jsonPath().getString("createdAt");
			res_createdAt = res_createdAt.substring(0, 11);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Status Code")
	public void validate_status_code() {
		Assert.assertEquals(response.statusCode(), 201);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Response Body Parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		req_name = jsp_req.getString("name");
		req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
	//    throw new io.cucumber.java.PendingException();
	}

}


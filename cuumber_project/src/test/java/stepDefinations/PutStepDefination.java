package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
//import junit.framework.Assert;
import org.testng.Assert;

public class PutStepDefination {
	String endpoint;
	String requestbody;
	File dir_name;
	Response response;
	String responce;
	int Statuscode;
	String res_name;
	String res_job;
	String res_id;
	String req_name;
	String req_job;
	String expecteddate;
	String res_time;
	String res_updatedAt;
	ResponseBody res_body;
	
	

	@Given("Name and Job in Request Body in Put API")
	public void name_and_job_in_request_body_in_put_api() throws IOException {
		dir_name = Utility.CreateLogDirectory("Put_API_Logs");
		requestbody = RequestBody.req_put_tc("Put_TC2");
		endpoint = RequestBody.Hostname() + RequestBody.Resource_Put();
		response = API_Trigger.Put_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestbody,
				endpoint);
		
		Utility.evidenceFileCreator(Utility.testLogName("Put_API"), dir_name, endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asString());
		
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Request with PayLoad to the endpoint in Put API")
	public void send_the_request_with_pay_load_to_the_endpoint_in_put_api() {
		Statuscode = response.getStatusCode();
		res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
	//	throw new io.cucumber.java.PendingException();

	}

	@Then("Validate Status Code in Put API")
	public void validate_status_code_in_put_api() {
		Assert.assertEquals(Statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Response Body Parameters in Put API")
	public void validate_response_body_parameters_in_put_api() {
		JsonPath jsp_req = new JsonPath(requestbody);
		req_name = jsp_req.getString("name");
		req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
		// throw new io.cucumber.java.PendingException();
	}
}

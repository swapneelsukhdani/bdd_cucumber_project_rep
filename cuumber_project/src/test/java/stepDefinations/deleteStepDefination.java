package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class deleteStepDefination {

	
	int statusCode;
	String Endpoint;
	Response response;
	File dir_name;

	@Given("Name and Job in Request Body in delete")
	public void name_and_job_in_request_body_in_delete() throws IOException {
		dir_name = Utility.CreateLogDirectory("Delete_API_Logs");
		Endpoint = Environment.Hostname() + Environment.Resource_Delete();
		response = API_Trigger.Delete_trigger(Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Delete_API"), dir_name, Endpoint, null,
				response.getHeader("Date"), null);
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Request with PayLoad to the endpoint in delete")
	public void send_the_request_with_pay_load_to_the_endpoint_in_delete() {
		statusCode = response.getStatusCode();
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Status Code in delete")
	public void validate_status_code_in_delete() {
		Assert.assertEquals(statusCode, 204);
		// throw new io.cucumber.java.PendingException();
	}
}

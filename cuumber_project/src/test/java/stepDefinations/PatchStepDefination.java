package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


public class PatchStepDefination {

	int statuscode;
	Response response;
	ResponseBody res_body;
	
	
	//String res_body;
	String res_id;
	String res_updatedAt;
	String req_name;
	String Endpoint;
	File dir_name;
	String requestBody;
	String req_job;
	String res_name;
	String res_job;
	
	
	@Given("Name and Job in Request Body in patch")
	public void name_and_job_in_request_body_in_patch() throws IOException {
		dir_name = Utility.CreateLogDirectory("Patch_API_Logs");
		requestBody = RequestBody.req_patch_tc("Patch_TC1");
		Endpoint = Environment.Hostname() + Environment.Resource_Patch();


		response = API_Trigger.Patch_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Patch_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

			  //  throw new io.cucumber.java.PendingException();
	}
		
	
	@When("Send the Request with PayLoad to the endpoint in patch")
	public void send_the_request_with_pay_load_to_the_endpoint_in_patch() {
		statuscode = response.statusCode();
		res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		String res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Status Code in patch")
	public void validate_status_code_in_patch() {
		Assert.assertEquals(statuscode, 200);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Response Body Parameters in patch")
	public void validate_response_body_parameters_in_patch() {
		
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);


	  //  throw new io.cucumber.java.PendingException();
	}
}
